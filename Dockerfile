FROM fedora:40

RUN dnf install -y python3-pip git nodejs; dnf clean all

RUN pip install --no-cache-dir jupyterlab ipywidgets plotly pandas scikit-learn scikit-image jupyterlab-git matplotlib

ENTRYPOINT ["jupyter", "lab", "--ip=0.0.0.0", "--allow-root", "--no-browser", "--notebook-dir=/app"]

